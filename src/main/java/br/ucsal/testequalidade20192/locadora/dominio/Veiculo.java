package br.ucsal.testequalidade20192.locadora.dominio;

import br.ucsal.testequalidade20192.locadora.dominio.enums.SituacaoVeiculoEnum;

public class Veiculo {

	private String placa;

	private Integer ano;

	private Modelo modelo;

	private Double valorDiaria;

	private SituacaoVeiculoEnum situacao;

	public Veiculo(String placa, Integer ano, Modelo modelo, Double valorDiaria) {
		super();
		situacao = SituacaoVeiculoEnum.DISPONIVEL;
		this.placa = placa;
		this.ano = ano;
		this.modelo = modelo;
		this.valorDiaria = valorDiaria;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public Double getValorDiaria() {
		return valorDiaria;
	}

	public void setValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
	}

	public SituacaoVeiculoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
	}

}
